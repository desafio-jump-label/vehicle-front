import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ListResult } from '../shared/models/listResult';


@Injectable({
  providedIn: 'root'
})
export class RestApiService<T> {
  protected readonly apiUrl = `${this.baseUrl}/${this.entityname}`;

  constructor( 
    protected readonly http: HttpClient,
    protected readonly entityname: String,
    protected readonly baseUrl: String = environment.BASE_URL
    ) { }

  httpOptions = {
    Headers: new HttpHeaders({
      'Content-Type': 'applications/json'
    })
  }

  fetchEntities(query?: { [ key: string]: string | string[] }): Observable<ListResult<T[]>>{
    return this.http.get<ListResult<T[]>>(this.apiUrl, { params: query });
  }

  getByPlate(filter: string): Observable<T>{
    console.log(`${this.apiUrl}/filter=${filter}`)
    return this.http.get<T>(`${this.apiUrl}/filter=${filter}`);
  }

  getAllByStatus(filter: string): Observable<ListResult<T[]>>{
    return this.http.get<ListResult<T[]>>(`${this.apiUrl}?filter=${filter}`);
  }

  getEntities(id: number): Observable<T>{
    const url = this.entityUrl(id)
    return this.http.get<T>(url);
  }

  createEntities(body: T): Observable<T>{
    return this.http.post<T>(this.apiUrl, body);
  }

  updateEntities(id: number, body: T): Observable<T>{
    const url = this.entityUrl(id)
    return this.http.put<T>(url, body);
  }

  deleteEntities(id: number): Observable<T>{
    const url = this.entityUrl(id);
    return this.http.delete<T>(url);
  }

  protected entityUrl(id: number): string {
    return [this.apiUrl, id].join('/');
  }

}
