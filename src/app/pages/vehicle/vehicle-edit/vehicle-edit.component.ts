import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Vehicle } from 'src/app/shared/models/vehicle';
import { VehicleService } from 'src/app/shared/services/vehicle.service';

@Component({
  selector: 'app-vehicle-edit',
  templateUrl: './vehicle-edit.component.html',
  styleUrls: ['./vehicle-edit.component.scss']
})
export class VehicleEditComponent implements OnInit {
  vehicle!: Vehicle
  vehicleForm!: FormGroup;
  
  constructor(
    private route: ActivatedRoute,
    public router: Router,
    private service: VehicleService,
  ) { }

  ngOnInit(): void {
    const plate = this.route.snapshot.params['plate'];
    this.service.getByPlate(plate).pipe().subscribe((data) => {
      this.vehicle = data;
      console.log(data)
    });

    this.vehicleForm = new FormGroup({
      plate: new FormControl('', [Validators.required]),
      model: new FormControl('', [Validators.required]),
      manufacturer: new FormControl('', [Validators.required]),
      color: new FormControl('', [Validators.required])
    });
  }

  get f(){
    return this.vehicleForm.controls
  }

  submit(){
    console.log(this.vehicleForm.value)
    this.service.update(this.vehicle.id, this.vehicleForm.value).subscribe((data: {}) => {
      console.log('Post updated successfully')
      this.router.navigateByUrl('/vehicle')
    })
  }


}
