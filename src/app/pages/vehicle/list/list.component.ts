import { AfterViewInit, ViewChild, Component, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ListResult } from 'src/app/shared/models/listResult';
import { Vehicle } from 'src/app/shared/models/vehicle';
import { VehicleService } from 'src/app/shared/services/vehicle.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogDeleteComponent } from 'src/app/shared/components/dialog-delete/dialog-delete.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements AfterViewInit   {

  vehicles!: ListResult<Vehicle[]>;
  
  displayedColumns: string[] = ['plate', 'model', 'manufacturer', 'status', 'actions'];
  displayedStaticColumns: string[] = ['actions'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;

  constructor(
    private service: VehicleService, 
    public dialog: MatDialog){}

  ngAfterViewInit(): void {
    this.getAll();
  }
  
  getAll(){
    this.service.getAllBy().pipe().subscribe((data) => { 
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.paginator = this.paginator;
    });    
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialogDelete(vehicle?: Vehicle): void {
    const dialogRef = this.dialog.open(DialogDeleteComponent, {
      width: '250px',
      data: vehicle
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if(result != undefined){
        this.service.delete(result).pipe().subscribe(() => { this.getAll(); });
      }
    });
  }
}
