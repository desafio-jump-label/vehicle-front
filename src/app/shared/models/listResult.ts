export interface ListResult<T> {
    content: T[],
    Pageable: {

    },
    totalPages: number,
    size: number,
    totalElements: number,
    number: number
}



