import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Vehicle } from '../models/vehicle'
import { RestApiService } from '../../config/rest-api.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  vehicleService: RestApiService<Vehicle>;
  constructor(protected http: HttpClient) {
    this.vehicleService = new RestApiService<Vehicle>(this.http, 'vehicle');
  }

  getAllBy(params?: string){
    return this.vehicleService.fetchEntities();
  }

  get(id: number){
    return this.vehicleService.getEntities(id);
  }
  
  getByPlate(plate: string){
    return this.vehicleService.getByPlate(plate);
  }

  save(vehicle: Vehicle){
    return this.vehicleService.createEntities(vehicle);
  }

  update(id: number, body: Vehicle){
    return this.vehicleService.updateEntities(id, body);
  }

  delete(id: number){
    return this.vehicleService.deleteEntities(id);
  }

}
