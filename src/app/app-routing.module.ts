import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './pages/vehicle/list/list.component';
import { VehicleEditComponent } from './pages/vehicle/vehicle-edit/vehicle-edit.component';

const routes: Routes = [
  { path: 'vehicle', component: ListComponent },
  { path: 'vehicle/:plate', component: VehicleEditComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
